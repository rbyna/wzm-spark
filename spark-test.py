#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Aug 24 16:16:47 2018

@author: ravitejarajabyna
"""

from __future__ import print_function

import sys

from pyspark import SparkContext
from pyspark.streaming import StreamingContext
from pyspark.streaming.kafka import KafkaUtils


if __name__ == "__main__":
    #if len(sys.argv) != 3:
    #    print("Usage: kafka_wordcount.py <zk> <topic>", file=sys.stderr)
    #    sys.exit(-1)
    #spark = SparkSession.builder.master("local").appName("Word Count").getOrCreate()
    #sc = SparkContext(master, appName)
    
    sc = SparkContext(appName="PythonStreamingKafkaWordCount")
    conf = sc.getConf()
    conf.set("spark.driver.extraClassPath","/Library/Java/JavaVirtualMachines/jdk1.8.0_151.jdk")
    ssc = StreamingContext(sc, 1)
    
    #zkQuorum, topic = sys.argv[1:]
    kvs = KafkaUtils.createStream(ssc, "localhost:2181", "spark-streaming-consumer", {"test": 1})
    lines = kvs.map(lambda x: x[1])
    print(lines.count())
    
    counts = lines.flatMap(lambda line: line.split(" ")) \
        .map(lambda word: (word, 1)) \
        .reduceByKey(lambda a, b: a+b)

    es_rdd = sc.newAPIHadoopRDD(inputFormatClass="org.elasticsearch.hadoop.mr.EsInputFormat", keyClass="org.apache.hadoop.io.NullWritable", valueClass="org.elasticsearch.hadoop.mr.LinkedMapWritable", conf={ "es.resource" : "test/_doc" })
    
    print(es_rdd.first())
    value_counts = es_rdd.map(lambda item: item[1]["message"])
    value_counts = value_counts.map(lambda word: (word, 1))
    value_counts = value_counts.reduceByKey(lambda a, b: a+b)
# put the results in the right format for the adapter
    value_counts = value_counts.map(lambda item: ('key', { 'field': 'message', 'val': item[0], 'count': item[1] }))
    # write the results to "titanic/value_counts"
    value_counts.saveAsNewAPIHadoopFile(path='-', outputFormatClass="org.elasticsearch.hadoop.mr.EsOutputFormat", keyClass="org.apache.hadoop.io.NullWritable", valueClass="org.elasticsearch.hadoop.mr.LinkedMapWritable", conf={ "es.resource" : "test/_doc" })


    file = open("testfile","w") 
 
    file.write("Hello World") 
    file.write("This is our new text file") 
    file.write("and this is another line.") 
    file.write("Why? Because we can.") 
 
    file.close() 
    print("Hello")
    counts.pprint()
    
    ssc.start()
    ssc.awaitTermination()
